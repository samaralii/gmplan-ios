
import Foundation
import UIKit
import Alamofire
import MBProgressHUD

class StudentListVC: UIViewController {
    
    @IBOutlet var tableViewStudent: UITableView!
    
    public var studentList = [Student_]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initz()
    }
    
    func showProgress() {
        let loading = MBProgressHUD.showAdded(to: view, animated: true)
        loading.mode = MBProgressHUDMode.indeterminate
        loading.label.text = "Loading..."
    }
    
    
    func hideProgress() {
        MBProgressHUD.hideAllHUDs(for: view, animated: true)
    }
    
    
    func initz() {
//        let textAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
//        UINavigationBar.appearance().titleTextAttributes = textAttributes
        
        
        tableViewStudent.delegate = self
        tableViewStudent.dataSource = self
        
        if studentList.count > 0 {
            self.tableViewStudent.reloadData()
        }
        
        
    }
    
    
    func getStudentDetail(_ id: String) {
        showProgress()
        
        if let staffId = _getUserData()?.UserObject?.AdmissionStaff?.id,
            let collegeId = _getUserData()?.UserObject?.AdmissionStaff?.college_id {
            
              showProgress()
            
            let url = "\(BASE_URL)students/staff_student_view"
            let params = ["staffid": staffId, "college_id": collegeId, "student_id": id]
            print("Url: \(url) Params: \(params)")
            
            Alamofire.request(url, method: .post, parameters: params).responseJSON {
                response in
                
                self.hideProgress()
                
                
                switch response.result {
                    
                case .success:
                    if let json = response.data {
                        
                        
                        do {
                            
                            let studentObj = try JSONDecoder().decode(StudentDetailObject.self, from: json)
                            
                            self.openDetailView(studentObj)
                            
                            
                        } catch let errorJson {
                            print("Error", errorJson)
                        }
                        
                        
                        
                    } else {
                        print("Error")
                    }
                    
                case .failure(let error):
                    print(error)
                }
            }
        }
    }
    
    
    func openDetailView(_ studentObj: StudentDetailObject){
        if studentObj.response == "success" {
            
            let studentDetailVC = storyboard?.instantiateViewController(withIdentifier: "idnStudentDetailVC") as! StudentDetailVC
            
            studentDetailVC.studentDetail = studentObj
            
            navigationController?.pushViewController(studentDetailVC, animated: true)
            
        }
    }
    
    
    
}


extension StudentListVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return studentList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let student = studentList[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "idnStudentCell") as! StudentCell
        cell.addItem(student.name)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.getStudentDetail(studentList[indexPath.row].id)
    }
    
    
}
