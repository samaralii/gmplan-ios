

import Foundation


struct StudentDetailObject: Decodable {
    var response: String
    var studentinfo: StudentInfo
//    var studentcolleges: [StudentColleges]
    var rating: [Rating]?
}

struct StudentInfo: Decodable {
    var Student: StudentObj
    var Career: [_Career]?
}

struct StudentObj: Decodable {
    var id: String?
    var first_name: String?
    var middle_name: String?
    var last_name: String?
    var grade: String?
    var ugpa: String?
    var wgpa: String?
    var class_rank: String?
    var views: String?
    var reco: String?
    var sat_score: String?
    var user_id: String?
    var cons_id: String?
    var info: String?
    var photo: String?
    var photo_dir: String?
    var my_likes: String?
    var my_dislike: String?
    var my_music: String?
    var my_team: String?
    var my_college: String?
    var my_school: String?
    var school_id: String?
    var step: String?
    var act_score: String?
    var city_id: String?
    var country_id: String?
    var state_id: String?
    var organization: String?
    var fullname: String?
}

struct StudentColleges: Decodable {
}

struct _Career: Decodable {
    var career_title: String?
}

struct Rating: Decodable {
    var area: String?
    var score: String?
    var description: String?
}
