
import Foundation
class Feed{
    
    var id: String?
    var detail: String?
    var college_id: String?
    var user_id: String?
    var image: String?
    var created: String?
    var update: String?
    var collegename: String?
    var user: String?


    init(id: String?,
         detail: String?,
         college_id: String?,
         user_id: String?,
         image: String?,
         created: String?,
         update: String?,
         collegename: String?,
         user: String?) {
        
        self.id = id
        self.detail = detail
        self.college_id = college_id
        self.user_id = user_id
        self.image = image
        self.created = created
        self.update = update
        self.collegename = collegename
        self.user = user
        
    }

}


struct NewsFeed: Decodable {
    let detail: String?
    let image: String?
    let collegename: String?
    let user: String?
    
}
