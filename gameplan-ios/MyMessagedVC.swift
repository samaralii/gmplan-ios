import Foundation
import Alamofire
import MBProgressHUD


class MyMessagesVC: UIViewController {
    
    @IBOutlet weak var segCntrl: UISegmentedControl!
    @IBOutlet weak var msgTableView: UITableView!
    
    private var inboxList = [MessagesObj]()
    private var outboxList = [MessagesObj]()
    
    @IBOutlet weak var viewContainer: UIView!
    var inboxView: UIView!
    var outboxView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initz()
    }
    
    private func initz() {
        getInboxList()
    }
    
    func showProgress() {
        let loading = MBProgressHUD.showAdded(to: view, animated: true)
        loading.mode = MBProgressHUDMode.indeterminate
        loading.label.text = "Loading..."
    }
    
    
    func hideProgress() {
        MBProgressHUD.hideAllHUDs(for: view, animated: true)
    }
    
    @IBAction func SegmentedAction(_ sender: UISegmentedControl) {
        print(sender.selectedSegmentIndex)
        switch sender.selectedSegmentIndex {
        case 0:
            clearList()
            getInboxList()
            break
        case 1:
            clearList()
            getOutboxList()
            break
        default:
            break
        }
    }
    
    private func clearList() {
        inboxList.removeAll()
        outboxList.removeAll()
        msgTableView.reloadData()
    }
    
    
    
    private func getInboxList() {
        
        if let collegeId = _getUserData()?.UserObject?.AdmissionStaff?.college_id {
            showProgress()
            
            let url = "\(BASE_URL)students/staff_inbox"
            let params = ["collegeId": collegeId]
            print("Url: \(url) Params: \(params)")
            
            
            Alamofire.request(url, method: .post, parameters: params).responseJSON {
                response in
                
                self.hideProgress()
                
                
                switch response.result {
                    
                case .success:
                    if let json = response.data {
                        
                        do {
                            
                            self.inboxList = try JSONDecoder().decode([MessagesObj].self, from: json)
                            
                            if self.inboxList.count > 0 {
                                self.msgTableView.reloadData()
                            }
                            
                            
                        } catch let errorJson {
                            print("Error", errorJson)
                        }
                        
                        
                        
                    } else {
                        print("Error")
                    }
                    
                case .failure(let error):
                    print(error)
                }
            }
            
        }
    }
    
    
    private func getOutboxList() {
        
        if let staffId = _getUserData()?.UserObject?.AdmissionStaff?.id {
            showProgress()
            
            let url = "\(BASE_URL)students/staff_outgoing"
            let params = ["staffId": staffId]
            print("Url: \(url) Params: \(params)")
            
            
            Alamofire.request(url, method: .post, parameters: params).responseJSON {
                response in
                
                self.hideProgress()
                
                switch response.result {
                    
                case .success:
                    if let json = response.data {
                        
                        do {
                            
                            self.outboxList = try JSONDecoder().decode([MessagesObj].self, from: json)
                            
                            if self.outboxList.count > 0 {
                                self.msgTableView.reloadData()
                            }
                            
                            
                        } catch let errorJson {
                            print("Error", errorJson)
                        }
                        
                        
                    } else {
                        print("Error")
                    }
                    
                case .failure(let error):
                    print(error)
                }
            }
            
        }
    }
    
}

extension MyMessagesVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch segCntrl.selectedSegmentIndex {
        case 0:
            return inboxList.count
        case 1:
            return outboxList.count
        default:
            return 0
        }
        
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch segCntrl.selectedSegmentIndex {
            
        case 0:
            let data = inboxList[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: "idnNewInboxCell") as! NewInboxCell
            cell.addItem(data)
            return cell
            
        case 1:
            let data = outboxList[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: "idnNewOutboxCell") as! NewOutboxCell
            cell.addItem(data)
            return cell
            
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "idnNewOutboxCell") as! NewOutboxCell
            return cell
        }
        
    }
    
    
}
