

import Foundation
import UIKit

struct ListItems {
    var title: String
    var id: Int
}

class StudentDetailVC: UIViewController {
    
    @IBOutlet var ivProfileImage: UIImageView!
    @IBOutlet var tableViewMenu: UITableView!
    @IBOutlet var topView: UIView!
    
    public var studentDetail: StudentDetailObject?
    
    private var menuItems = [ListItems]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initz()
    }
    
    func initz() {
        
        tableViewMenu.dataSource = self
        tableViewMenu.delegate = self
        
        if let photo = studentDetail?.studentinfo.Student.photo,
            let photoDir = studentDetail?.studentinfo.Student.photo_dir {
            ivProfileImage.sd_setImage(with: getProfileImgUrl(photoDir, photo).toUrl)
        } else {
            ivProfileImage.sd_setImage(with: IMG_PLACEHOLDER.toUrl)
        }
        
        
        if let name = studentDetail?.studentinfo.Student.fullname {
            menuItems = createListItems(name)
            tableViewMenu.reloadData()
        }
        
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let shadowPath = UIBezierPath(rect: topView.bounds)
        topView.layer.masksToBounds = false
        topView.layer.shadowColor = UIColor.black.cgColor
        topView.layer.shadowOffset = CGSize(width: 0, height: 1)
        topView.layer.shadowOpacity = 0.5
        topView.layer.shadowPath = shadowPath.cgPath
    }
    
    func createListItems(_ name: String) -> [ListItems] {
        var items = [ListItems]()
        items.append(ListItems.init(title: "About \(name)", id: 0))
        items.append(ListItems.init(title: "Videos", id: 1))
        items.append(ListItems.init(title: "Achievements", id: 2))
        items.append(ListItems.init(title: "Favorite Colleges", id: 3))
        items.append(ListItems.init(title: "Semester Goals", id: 4))
        items.append(ListItems.init(title: "My Careers", id: 5))
        items.append(ListItems.init(title: "Interest Profile", id: 6))
        return items
    }
    
    func onMenuItemClick(_ id: Int) {
        switch id {
        case 0:
            openAboutView()
        case 5:
            openCareerListView()
        case 6:
            openInterestProfileView()
            
        default: break
        }
    }
    
    func openAboutView() {
        let VC = storyboard?.instantiateViewController(withIdentifier: "idnAboutStudentVC") as! AboutStudentVC
        
        VC.studentDetail = studentDetail
        navigationController?.pushViewController(VC, animated: true)
    }
    
    func openInterestProfileView() {
        if let rating = studentDetail?.rating {
            if rating.count > 0 {
                let VC = storyboard?.instantiateViewController(withIdentifier: "idnInterestProfileVC") as! InterestProfileVC
                VC.ratings = rating
                navigationController?.pushViewController(VC, animated: true)
            }
        }
    }
    
    func openCareerListView() {
        if let careers = studentDetail?.studentinfo.Career {
            if careers.count > 0 {
                let VC = storyboard?.instantiateViewController(withIdentifier: "idnStudentCareerVC") as! StudentCareerVC
                VC.careers = careers
                navigationController?.pushViewController(VC, animated: true)
            }
        }
        
    }
    
}


extension StudentDetailVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let menu = menuItems[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "idnMenuCell") as! MenuCell
        cell.addItem(title: menu.title)
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.onMenuItemClick(menuItems[indexPath.row].id)
    }
    
    
}
