import Foundation

struct UserObject: Decodable {
    var response: String
    var UserObject: _UserData?
}

struct _UserData: Decodable {
    var User: _User?
    var AdmissionStaff: _AdmissionStaff?
    var Student: StudentObj?
}

struct _User: Decodable {
    var id: String?
    var username: String?
    var type: String?
    var userid: String?
}

struct _AdmissionStaff: Decodable {
    var id: String?
    var name: String?
    var middle_name: String?
    var job_tittle: String?
    var user_id: String?
    var college_id: String?
}
