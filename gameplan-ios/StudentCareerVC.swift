
import Foundation


class StudentCareerVC: UIViewController {
   
    public var careers = [_Career]()
    
    @IBOutlet var tableViewCareer: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initz()
    }
    
    private func initz() {
        tableViewCareer.delegate = self
        tableViewCareer.dataSource = self
        tableViewCareer.reloadData()
    }
    
}


extension StudentCareerVC: UITableViewDelegate, UITableViewDataSource {
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return careers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let career = careers[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "idnCareerCell") as! CareerCell
        cell.addItem(career)
        return cell
    }
}


