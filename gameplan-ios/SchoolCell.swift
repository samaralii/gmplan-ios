
import Foundation
import UIKit


class SchoolCell: UITableViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var view: UIView!
    
    func addItem(name: String) {
        
        view.layer.cornerRadius = 10
        view.layer.masksToBounds = true
        
        lblName.text = name
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
}
