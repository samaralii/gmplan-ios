

import Foundation
import UIKit

class StudentCareerCell: UITableViewCell {

    
    @IBOutlet weak var lblCareerName: UILabel!
    
    
    func addItem(_ careerName: String) {
        
        lblCareerName.text = careerName
        
    }

}
