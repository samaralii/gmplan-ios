
import Foundation
import UIKit


class InterestProfileVC: UIViewController {
    
    @IBOutlet weak var tableViewIntProfile: UITableView!
    
    public var ratings = [Rating]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initz()
    }

    
    private func initz() {
        tableViewIntProfile.delegate = self
        tableViewIntProfile.dataSource = self
        tableViewIntProfile.reloadData()
    }
    
}


extension InterestProfileVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ratings.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let rating = ratings[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "idnInterestProfileCell") as! InterestProfileCell
        
        cell.addItem(rating)
        
        return cell
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tableViewIntProfile.estimatedRowHeight = 200
        tableViewIntProfile.rowHeight = UITableViewAutomaticDimension
    }
    
}
