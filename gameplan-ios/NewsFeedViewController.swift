import Foundation
import UIKit
import Alamofire
import MBProgressHUD

class NewsFeedViewController: UIViewController {
    
    var feedList = [NewsFeed]()
    
    @IBOutlet weak var feedTableView: UITableView!
    
    override func viewDidLoad() {
        
        showProgress()
        getUserFeed()
    }
    
    func showProgress() {
        let loading = MBProgressHUD.showAdded(to: view, animated: true)
        loading.mode = MBProgressHUDMode.indeterminate
        loading.label.text = "Getting Feeds"
    }
    
    func hideProgress() {
        MBProgressHUD.hideAllHUDs(for: view, animated: true)
    }
    
    func getUserFeed() {
        
        let url = "\(BASE_URL)AdmissionStaffs/staff_newsfeeds"
        let parameters = ["key": "yes"]
        
        Alamofire.request(url, method: .post,parameters: parameters).responseJSON { response in
            
            
            self.hideProgress()
            
            switch response.result{
                
            case .success:
                if let data = response.data {
                    
                    do {
                        
                        self.feedList = try JSONDecoder().decode([NewsFeed].self, from: data)
                        
                        if self.feedList.count > 0 {
                            self.feedTableView.reloadData()
                        }
                        
                    } catch let errorJson {
                        print("Error While Parsing Json", errorJson)
                    }
                }
                
            case .failure(let error):
                print(error)
            }
        }
    }
}




extension NewsFeedViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return feedList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let feed = feedList[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "FeedCell") as! FeedCell
        cell.addItem(feed)
        return cell
    }
    
    override func viewWillAppear(_ animated: Bool) {
        feedTableView.estimatedRowHeight = 400
        feedTableView.rowHeight = UITableViewAutomaticDimension
    }
    
}
