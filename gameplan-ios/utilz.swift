import Foundation

public func getUserData(forKey: String, key: String) -> String? {
    let userData = UserDefaults.standard.data(forKey: forKey)
    if let data = userData {
        let dict = NSKeyedUnarchiver.unarchiveObject(with: data) as! [String:Any]
//        print(dict)
        return dict[key] as? String
    } else {
        return nil
    }
}

public func getProfileImgUrl(_ photoDir: String, _ photo: String) -> String {
    let url = "\(IMG_URL_PROFILE_IMG)\(photoDir)/\(photo)"
    print(url)
    return url
}


func _getUserData() -> UserObject? {
    do {
        let userData = UserDefaults.standard.object(forKey: KEY_USERDATA) as? Data
        if let data = userData {
            return try JSONDecoder().decode(UserObject.self, from: data)
        } else {
            return nil
        }
    } catch let errorJson {
        print("Error While Parsing Json", errorJson)
        return nil
    }
}



extension String {
    
    var toUrl: URL { return URL(string: self)! }
    
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!,
                                          options: [.documentType: NSAttributedString.DocumentType.html,
                                                    .characterEncoding: String.Encoding.utf8.rawValue],
                                          documentAttributes: nil)
        } catch {
            print("error: ", error)
            return nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
    
}

