

import Foundation
import UIKit
import SwiftyJSON

class StudentDetail: UIViewController{
    
    @IBOutlet weak var lblImageView: UIImageView!
    
    @IBOutlet weak var lblAbout: UILabel!
    
    @IBOutlet weak var lblGrade: UILabel!
    
    @IBOutlet weak var lblUgpa: UILabel!
    
    @IBOutlet weak var lblWgpa: UILabel!
    
    @IBOutlet weak var lblClassRank: UILabel!
    
    @IBOutlet weak var lblSatScore: UILabel!
    
    @IBOutlet weak var lblHighSchool: UILabel!
    
    @IBOutlet weak var lblLikes: UILabel!
    
    @IBOutlet weak var lblDislikes: UILabel!
    
    @IBOutlet weak var lblMusic: UILabel!
    
    @IBOutlet weak var lblTeam: UILabel!
    
    @IBOutlet weak var lblColleges: UILabel!
    
    @IBOutlet weak var careerTableView: UITableView!
    
    @IBOutlet weak var scroller: UIScrollView!
    
 
    @IBOutlet weak var interestProfileTableView: UITableView!

    
    var studentJson: Any? = nil
    
    var arrCareer = [[String:AnyObject]]()
    var arrIntProfile = [[String:AnyObject]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        careerTableView.dataSource = self
        careerTableView.delegate = self
//        careerTableView.register(StudentCareerCell.self, forCellReuseIdentifier: "studentCareerIdn")
        
        interestProfileTableView.dataSource = self
        interestProfileTableView.delegate = self
//        interestProfileTableView.register(InterestProfileCell.self, forCellReuseIdentifier: "idnInterestProfile")
        
        
        if let data = studentJson {
            let json = JSON(data)
            
            let studentInfo = json["studentinfo"]
            setAboutStudentFields(data: studentInfo["Student"])
            
            if let careers = studentInfo["Career"].arrayObject {
                self.arrCareer = careers as! [[String:AnyObject]]
            }
            
            if self.arrCareer.count > 0 {
                careerTableView.reloadData()
            }
            
            
            if let intProfile = json["rating"].arrayObject {
                self.arrIntProfile = intProfile as! [[String:AnyObject]]
            }
            
            if self.arrIntProfile.count > 0 {
                interestProfileTableView.reloadData()
                print("Rating Count : \(self.arrIntProfile.count)")
            }
            
            
            
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        scroller.contentSize = CGSize(width: 414, height: 2000)
    }
    
    override func viewDidLayoutSubviews() {
        scroller.contentSize = CGSize(width: 414, height: 2000)
    }
    
    
    
    private func setAboutStudentFields(data: JSON) {
        
        lblImageView.sd_setImage(with: IMG_PLACEHOLDER.toUrl)
        lblImageView.setShowActivityIndicator(true)
        
        lblAbout.text = "About \(data["first_name"])"
        
        if let value = data["grade"].string{
            lblGrade.text = value
        }
        
        if let value = data["ugpa"].string{
            lblUgpa.text = value
        }
        
        if let value = data["wgpa"].string{
            lblWgpa.text = value
        }
        
        if let value = data["class_rank"].string{
            lblClassRank.text = value
        }
        
        if let value = data["sat_score"].string{
            lblSatScore.text = value
        }
        
        if let value = data["my_school"].string{
            lblHighSchool.text = value
        }
        
        
        if let value = data["my_like"].string{
            lblLikes.text = value
        }
        
        
        if let value = data["my_dislike"].string{
            lblDislikes.text = value
        }
        
        
        if let value = data["my_music"].string{
            lblMusic.text = value
        }
        
        
        if let value = data["my_team"].string{
            lblTeam.text = value
        }
        
        
        if let value = data["my_college"].string{
            lblColleges.text = value
        }
        
        
        
        
    }
    

    
}


extension StudentDetail: UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        var count: Int?
        
        if self.careerTableView == tableView {
            count = arrCareer.count
        }
        
        if self.interestProfileTableView == tableView {
            count = arrIntProfile.count
        }
        
        
        return count!
        
        
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        var cell: UITableViewCell?
        
        
        if tableView == self.careerTableView {
            
            let dict = arrCareer[indexPath.row]
            
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "studentCareerIdn") as! StudentCareerCell
            
//            let cell1 = tableView.dequeueReusableCell(withIdentifier: "studentCareerIdn", for: indexPath) as! StudentCareerCell
            
            if let careerName = dict["career_title"] as? String {
                cell1.addItem(careerName)
            }
            
            cell = cell1
            
        }
        
        
        
        if tableView == self.interestProfileTableView {
            
            
            let dict = arrIntProfile[indexPath.row]
            
            let cell2 = tableView.dequeueReusableCell(withIdentifier: "idnInterestProfile") as! InterestProfileCell
//            
//            let cell2 = tableView.dequeueReusableCell(withIdentifier: "idnInterestProfile", for: indexPath) as! InterestProfileCell
            
            let area = dict["area"] as? String
            let score = dict["score"] as? String
            let des = dict["description"] as? String

            cell2.addItem(area, score, des)
           
            
            cell = cell2
        }
        
        
        return cell!

    }

    
}
