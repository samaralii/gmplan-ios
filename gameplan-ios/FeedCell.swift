
import UIKit

class FeedCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var ivImage: UIImageView!
    @IBOutlet weak var tvDetail: UITextView!
    
    
    func addItem(_ feed: NewsFeed) {
        
        if let detail = feed.detail {
            self.tvDetail.text = detail.html2String
        } else {
             self.tvDetail.text = ""
        }
        
        
        if let collegeName = feed.collegename {
            lblTitle.text = collegeName
        } else {
            lblTitle.text = ""
        }

        
        if let image = feed.image {
            ivImage.sd_setImage(with: URL(string: "\(IMG_BASE_URL)\(image)"))
            ivImage.setShowActivityIndicator(true)
        }
        
        if let username = feed.user {
            lblName.text = username
        }
    
        
    }
}
