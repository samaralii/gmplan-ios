
import Foundation
import UIKit

class AboutStudentVC: UIViewController {
    
    @IBOutlet weak var lblGrade: UILabel!
    @IBOutlet weak var lblUgpa: UILabel!
    @IBOutlet weak var lblWgpa: UILabel!
    @IBOutlet weak var lblClassRank: UILabel!
    @IBOutlet weak var lblActScore: UILabel!
    @IBOutlet weak var lblSatScore: UILabel!
    @IBOutlet weak var lblHighSchool: UILabel!
    @IBOutlet weak var lblLikes: UILabel!
    @IBOutlet weak var lblDislikes: UILabel!
    @IBOutlet weak var lblMusic: UILabel!
    @IBOutlet weak var lblTeam: UILabel!
    @IBOutlet weak var lblColleges: UILabel!
    @IBOutlet weak var topView: UIView!
    
    
    
    public var studentDetail: StudentDetailObject?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initz()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let shadowPath = UIBezierPath(rect: topView.bounds)
        topView.layer.masksToBounds = false
        topView.layer.shadowColor = UIColor.black.cgColor
        topView.layer.shadowOffset = CGSize(width: 0, height: 1)
        topView.layer.shadowOpacity = 0.5
        topView.layer.shadowPath = shadowPath.cgPath
    }
    
    private func initz() {
        
        
        
        if let val = studentDetail?.studentinfo.Student.grade {
            lblGrade.text = val
        } else {
            lblGrade.text = " "
        }
        
        if let val = studentDetail?.studentinfo.Student.ugpa {
            lblWgpa.text = val
        } else {
            lblWgpa.text = " "
        }
        
        if let val = studentDetail?.studentinfo.Student.wgpa {
            lblUgpa.text = val
        } else {
            lblUgpa.text = " "
        }
        
        if let val = studentDetail?.studentinfo.Student.class_rank {
            lblClassRank.text = val
        } else {
            lblClassRank.text = " "
        }
        
        if let val = studentDetail?.studentinfo.Student.act_score {
            lblActScore.text = val
        } else {
            lblActScore.text = " "
        }
        
        if let val = studentDetail?.studentinfo.Student.sat_score {
            lblSatScore.text = val
        } else {
            lblSatScore.text = " "
        }
        
        if let val = studentDetail?.studentinfo.Student.my_likes {
            lblLikes.text = val
        } else {
            lblLikes.text = " "
        }
        
        if let val = studentDetail?.studentinfo.Student.my_dislike {
            lblDislikes.text = val
        } else {
            lblDislikes.text = " "
        }
        
        if let val = studentDetail?.studentinfo.Student.my_music {
            lblMusic.text = val
        } else {
            lblMusic.text = " "
        }
        
        if let val = studentDetail?.studentinfo.Student.my_team {
            lblTeam.text = val
        } else {
            lblTeam.text = " "
        }
        
        if let val = studentDetail?.studentinfo.Student.my_college {
            lblColleges.text = val
        } else {
            lblColleges.text = " "
        }
        
       lblHighSchool.text = " "
        
    }
    
}
