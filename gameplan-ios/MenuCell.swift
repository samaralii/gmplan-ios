
import Foundation
import UIKit

class MenuCell: UITableViewCell {
    
    @IBOutlet var lblTitle: UILabel!
  
    
    func addItem(title: String) {
        lblTitle.text = title
    }
    
  
}
