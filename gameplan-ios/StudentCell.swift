
import Foundation
import UIKit


class StudentCell: UITableViewCell {
    
    @IBOutlet var lblName: UILabel!
    
    func addItem(_ name: String) {
        lblName.text = name
    }
    
}
