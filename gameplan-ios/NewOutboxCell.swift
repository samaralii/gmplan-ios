

import Foundation


class NewOutboxCell: UITableViewCell {
    
    @IBOutlet weak var lblSubject: UILabel!
    
    
    func addItem(_ data: MessagesObj) {
        if let val = data.subject {
            lblSubject.text = val
        }
    }
    
}

