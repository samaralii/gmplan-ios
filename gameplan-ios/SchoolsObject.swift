

import Foundation


struct AllSchool: Decodable {
    var School: School_
}

struct School_: Decodable {
    var id: String
    var name: String
    var Student: [Student_]?
}


struct Student_: Decodable {
    var id: String
    var name: String
}
