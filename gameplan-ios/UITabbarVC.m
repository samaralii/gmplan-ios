//
//  UITabBarController+UiTabbarVC.m
//  gameplan-ios
//
//  Created by Ali Naveed on 9/25/17.
//  Copyright © 2017 GAMEPLAN. All rights reserved.
//

#import "UITabbarVC.h"

@implementation UITabbarVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.delegate = self;
    
    
    
}

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController {
    
    NSArray *tabViewControllers = tabBarController.viewControllers;
    UIView * fromView = tabBarController.selectedViewController.view;
    UIView * toView = viewController.view;
    if (fromView == toView)
        return false;
    NSUInteger fromIndex = [tabViewControllers indexOfObject:tabBarController.selectedViewController];
    NSUInteger toIndex = [tabViewControllers indexOfObject:viewController];
    
    //    [UIView transitionFromView:fromView
    //                        toView:toView
    //                      duration:0.3
    //                       options: toIndex > fromIndex ? UIViewAnimationOptionTransitionCurlUp : UIViewAnimationOptionTransitionCurlDown
    //                    completion:^(BOOL finished) {
    //                        if (finished) {
    //                            tabBarController.selectedIndex = toIndex;
    //                        }
    //                    }];
    return true;
}


@end
