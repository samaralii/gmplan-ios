

import Foundation


class InterestProfileCell: UITableViewCell {
    
    @IBOutlet weak var lblArea: UILabel!
    @IBOutlet weak var lblScore: UILabel!
    @IBOutlet weak var tvDescription: UITextView!
    
    
    func addItem(_ rating: Rating) {
        
        if let val = rating.area {
            lblArea.text = val
        } else {
            lblArea.text = " "
        }
        
        if let val = rating.score {
            lblScore.text = val
        } else {
            lblScore.text = " "
        }
        
        if let val = rating.description {
            tvDescription.text = val
            
        } else {
            tvDescription.text = " "
        }
        
    }
    
    
}
