

import Foundation


class CareerCell: UITableViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    
    func addItem(_ career: _Career) {
        if let name = career.career_title {
            lblName.text = name
        }
    }
}
