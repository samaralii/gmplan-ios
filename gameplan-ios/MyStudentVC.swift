import Foundation
import UIKit
import Alamofire
import MBProgressHUD



class MyStudentVC: UIViewController {
    
    
    @IBOutlet weak var tableSchool: UITableView!
    
    var schoolList = [AllSchool]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableSchool.dataSource = self
        tableSchool.delegate = self
        getSchools()
    }
    
    func showProgress() {
        let loading = MBProgressHUD.showAdded(to: view, animated: true)
        loading.mode = MBProgressHUDMode.indeterminate
        loading.label.text = "Loading..."
    }
    
    func hideProgress() {
        MBProgressHUD.hideAllHUDs(for: view, animated: true)
    }
    
    private func getSchools() {
        
            if let staffId = _getUserData()?.UserObject?.AdmissionStaff?.id,
                let collegeId = _getUserData()?.UserObject?.AdmissionStaff?.college_id {
            
             showProgress()
            
            let url = "\(BASE_URL)students/staff_get_schools"
            
            let parameters = ["staffid": staffId, "college_id": collegeId]
            
            print("Url: \(url) Parameters: \(parameters)")
            
            
            Alamofire.request(url, method: .post, parameters: parameters).responseJSON { response in
                
                self.hideProgress()
                
                switch response.result {
                    
                case .success:
                    if let data = response.data {
                        
                        do {
                            
                            self.schoolList = try JSONDecoder().decode([AllSchool].self, from: data)
                            
                            if self.schoolList.count > 0 {
                                self.tableSchool.reloadData()
                            }
                            
                            
                        } catch let errorJson {
                            print("Error while parsing json ", errorJson)
                        }
                        
                    } else {
                        print("Error")
                    }
                    
                    
                case .failure(let error):
                    print(error)
                }
            }
        }
    }
    
    
}



extension MyStudentVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schoolList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let school = schoolList[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "idnSchoolCell") as! SchoolCell
        
        var studentCount = 0
        
        if let count = school.School.Student?.count {
            if count > 0 {
                studentCount = count
            }
        }
        
        let schoolName = "\(school.School.name) (\(studentCount))"
        
        cell.addItem(name: schoolName)
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let studentsList = schoolList[indexPath.row].School.Student
        
        if let data = studentsList, data.count > 0{
            
            let studentListVC = self.storyboard?.instantiateViewController(withIdentifier: "idnStudentListVC") as! StudentListVC
            
            studentListVC.studentList = data
            
            self.navigationController?.pushViewController(studentListVC, animated: true)
            
        }
        
        
    }
    
}

