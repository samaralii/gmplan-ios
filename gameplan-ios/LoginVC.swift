
import Foundation
import OneSignal
import Alamofire
import MBProgressHUD
import LocalAuthentication


class LoginVC: UIViewController {
    
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var swRemember: UISwitch!
    @IBOutlet weak var ivTouchId: UIImageView!
    @IBOutlet weak var viewTouchId: UIView!
    @IBOutlet weak var lblEnableTouchId: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initz()
    }
    
    func showProgress() {
        let loading = MBProgressHUD.showAdded(to: view, animated: true)
        loading.mode = MBProgressHUDMode.indeterminate
        loading.label.text = "Logging you in"
    }
    
    func hideProgress() {
        MBProgressHUD.hideAllHUDs(for: view, animated: true)
    }
    
    
    private func initz() {
        tfEmail.delegate = self
        tfPassword.delegate = self
        showTouchIdView(false)
        showEnableTouchId(true)
        setTouchIdClickGesture()
        
        let isTouchIdEnabled = UserDefaults.standard.bool(forKey: TOUCHID_ENABLED)
        
        if isTouchIdEnabled {
            showTouchIdView(true)
            showEnableTouchId(false)
            self.swRemember.setOn(true, animated: true)
        }
        
        
        
    }
    
    private func setTouchIdClickGesture() {
        
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(LoginVC.imageTapped))
        singleTap.numberOfTapsRequired = 1
        
        ivTouchId.isUserInteractionEnabled = true
        ivTouchId.addGestureRecognizer(singleTap)
        
    }
    
    @objc func imageTapped() {
        print("Touch Id Pressed")
        showTouchIdDialog()
        
    }
    
    private func showTouchIdView(_ show: Bool) {
        viewTouchId.isHidden = !show
    }
    
    private func showEnableTouchId(_ show: Bool) {
        swRemember.isHidden = !show
        lblEnableTouchId.isHidden = !show
    }
    
    @IBAction func btnLogin(_ sender: Any) {
        
        guard let email = tfEmail.text, !email.isEmpty else {
            return
        }
        
        guard let password = tfPassword.text, !password.isEmpty else {
            return
        }
        
        
        initOneSignal(email, password)
        
    }
    
    
    @IBAction func onSwitchStateChanged(_ sender: Any) {
//        if swRemember.isOn {
//            ShowTouchIdVIew(true)
//        } else {
//            ShowTouchIdVIew(false)
//        }
    }
    
    
    
    private func setDefaultCredentials(_ email: String,_ password: String)  {
        if swRemember.isOn {
            UserDefaults.standard.set(true, forKey: TOUCHID_ENABLED)
            UserDefaults.standard.set(email, forKey: USER_EMAIL)
            UserDefaults.standard.set(password, forKey: USER_PASSWORD)
            UserDefaults.standard.synchronize()
        } else {
            UserDefaults.standard.set(false, forKey: TOUCHID_ENABLED)
            UserDefaults.standard.removeObject(forKey: USER_EMAIL)
            UserDefaults.standard.removeObject(forKey: USER_PASSWORD)
            UserDefaults.standard.synchronize()
        }
    }
    
    private func login(_ email: String, _ password: String, _ playerid: String = "123456") {
        
        showProgress()
        
        let url = "\(BASE_URL)users/api_loginservice"
        let parameters = ["email" : email,
                          "password" : password,
                          "playerid" : playerid]
        
        print("Url \(url)")
        print("Parameters \(parameters)")
        
        Alamofire.request(url, method: .post, parameters: parameters).responseJSON { response in
            
            
            switch response.result {
                
            case .success:
                
                if let data = response.data {
                    
                    do {
                        
                        let userObj = try JSONDecoder().decode(UserObject.self, from: data)
                        
                        if userObj.response == "success" {
                            
                            self.clearUserDefaults()
                            self.setDefaultCredentials(email, password)
                            self.saveUserData(data)
                            self.openFeedView()
                            
                        } else {
                            print("Not Success")
                            self.onError("Error", ERROR_MESSAGE_02)
                        }
                        
                        
                    } catch let errorJson {
                        print("Error while parsing json ", errorJson)
                        self.onError("Error", ERROR_MESSAGE_01)
                    }
                    
                    
                    
                } else {
                    print("Error")
                    self.onError("Error", ERROR_MESSAGE_01)
                }
                
                
            case .failure(let error):
                print(error)
                self.onError("Error", ERROR_MESSAGE_01)
            }
            
            self.hideProgress()
            
        }
    }
    
    private func clearUserDefaults() {
        let defaults = UserDefaults.standard
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            defaults.removeObject(forKey: key)
            print("Delete UserDefaults")
        }
    }
    
    
    private func onError(_ title: String, _ msg: String) {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    private func saveUserData(_ data: Data) {
        UserDefaults.standard.set(data, forKey: KEY_USERDATA)
        UserDefaults.standard.synchronize()
    }
    
    private func openFeedView() {
        self.performSegue(withIdentifier: "staffDashboardSegue", sender: self)
    }
    
    
    private func initOneSignal(_ email: String, _ password: String) {
        let status: OSPermissionSubscriptionState = OneSignal.getPermissionSubscriptionState()
        _ = status.permissionStatus.hasPrompted
        _ = status.permissionStatus.status
        _ = status.subscriptionStatus.subscribed
        _ = status.subscriptionStatus.userSubscriptionSetting
        
        
        
        let userID = status.subscriptionStatus.userId
        
        var playerId = "0000"
        
        if let userId = userID {
            playerId = userId
        }
        
        print("playerid= \(playerId)")
        
        login(email, password, playerId)
        
        
    }
    
    
    @IBAction func btnFacebook(_ sender: Any) {
        
        
        
        
    }
    
    private func showTouchIdDialog() {
        
        
        // 1. Create a authentication context
        let authenticationContext = LAContext()
        var error:NSError?
        
        // 2. Check if the device has a fingerprint sensor
        // If not, show the user an alert view and bail out!
        guard authenticationContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) else {
            
            showAlertViewIfNoBiometricSensorHasBeenDetected()
            return
            
        }
        
        // 3. Check the fingerprint
        authenticationContext.evaluatePolicy(
            .deviceOwnerAuthenticationWithBiometrics,
            localizedReason: TOUCH_ID_MSG,
            reply: { [unowned self] (success, error) -> Void in
                
                if( success ) {
                    
                    // Fingerprint recognized
                    // Go to view controller
                    self.navigateToAuthenticatedViewController()
                    
                }else {
                    
                    // Check if there is an error
                    if let err = error {
                        
                        let message = self.errorMessageForLAErrorCode(errorCode: err._code)
                        self.showAlertViewAfterEvaluatingPolicyWithMessage(message: message)
                        
                    }
                    
                }
                
        })
        
    }
    
    
    /**
     This method will present an UIAlertViewController to inform the user that the device has not a TouchID sensor.
     */
    func showAlertViewIfNoBiometricSensorHasBeenDetected(){
        
        showAlertWithTitle(title: "Error", message: "This device does not have a TouchID sensor.")
        
    }
    
    /**
     This method will present an UIAlertViewController to inform the user that there was a problem with the TouchID sensor.
     
     - parameter error: the error message
     
     */
    func showAlertViewAfterEvaluatingPolicyWithMessage( message:String ){
        
        showAlertWithTitle(title: "Error", message: message)
        
    }
    
    /**
     This method presents an UIAlertViewController to the user.
     
     - parameter title:  The title for the UIAlertViewController.
     - parameter message: The message for the UIAlertViewController.
     
     */
    func showAlertWithTitle( title:String, message:String ) {
        
        let alertVC = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alertVC.addAction(okAction)
        
        DispatchQueue.main.async() { () -> Void in
            
            self.present(alertVC, animated: true, completion: nil)
            
        }
        
    }
    
    /**
     This method will return an error message string for the provided error code.
     The method check the error code against all cases described in the `LAError` enum.
     If the error code can't be found, a default message is returned.
     
     - parameter errorCode: the error code
     - returns: the error message
     */
    func errorMessageForLAErrorCode( errorCode:Int ) -> String{
        
        var message = ""
        
        switch errorCode {
            
        case LAError.appCancel.rawValue:
            message = "Authentication was cancelled by application"
            
        case LAError.authenticationFailed.rawValue:
            message = "The user failed to provide valid credentials"
            
        case LAError.invalidContext.rawValue:
            message = "The context is invalid"
            
        case LAError.passcodeNotSet.rawValue:
            message = "Passcode is not set on the device"
            
        case LAError.systemCancel.rawValue:
            message = "Authentication was cancelled by the system"
            
        case LAError.touchIDLockout.rawValue:
            message = "Too many failed attempts."
            
        case LAError.touchIDNotAvailable.rawValue:
            message = "TouchID is not available on the device"
            
        case LAError.userCancel.rawValue:
            message = "The user did cancel"
            
        case LAError.userFallback.rawValue:
            message = "The user chose to use the fallback"
            
        default:
            message = "Did not find error code on LAError object"
            
        }
        
        return message
        
    }
    
    
    /**
     This method will push the authenticated view controller onto the UINavigationController stack
     */
    func navigateToAuthenticatedViewController(){
        print("Finger print success")
        self.openFeedView()
    }
    
}

extension LoginVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
