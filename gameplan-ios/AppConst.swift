

import Foundation

//Urls
let BASE_URL = "https://gmplan.com/"
let IMG_BASE_URL = "https://gmplan.com/img/feedsphotos/"
let IMG_URL_PROFILE_IMG = "https://www.gmplan.com/app/webroot"
let IMG_PLACEHOLDER = "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ad/Placeholder_no_text.svg/480px-Placeholder_no_text.svg.png"

//Keys
let KEY_USERDATA = "userdata"
let TOUCHID_ENABLED = "rememberme"
let USER_EMAIL = "email"
let USER_PASSWORD = "password"

//Messages
let ERROR_MESSAGE_01 = "An unknown error has occured"
let ERROR_MESSAGE_02 = "Email or password is incorrect"
let TOUCH_ID_MSG = "Authenticating via Touch ID"
