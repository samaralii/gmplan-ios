

import Foundation


class NewInboxCell: UITableViewCell {
    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var lblSubject: UILabel!
    
    
    func addItem(_ data: MessagesObj) {
        if let val = data.subject {
            lblSubject.text = val
        }
    }
    
}
